﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MainLib;
using MainLib.AppSettings;

namespace Caller
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Caller: " + Settings.Modules.UpTarget);
            var x = new CMainLib();
            try
            {
                x.Pick(Settings.Modules.UpTarget, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: "+ex.Message);
            }

            Console.ReadLine();
        }
    }
}
