﻿using System.Collections.Generic;

namespace MainLib.AppSettings
{
    public class Settings
    {
        public class Modules
        {
            static Modules()
            {
                List = new List<string>{UpTarget, StatDb, MainDb};
            }

            public const string UpTarget = "UpTarget";
            public const string StatDb = "StatDb";
            public const string MainDb = "MainDb";

            public static List<string> List;
        }
    }
}