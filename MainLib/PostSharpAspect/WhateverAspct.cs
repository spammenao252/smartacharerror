﻿using System;
using PostSharp.Aspects;
using PostSharp.Aspects.Dependencies;

namespace MainLib.PostSharpAspect
{
    [Serializable]
    [AspectRoleDependency(AspectDependencyAction.Order, AspectDependencyPosition.After, StandardRoles.Tracing)]
    class WhateverAspct : OnExceptionAspect
    {
        /// <exception cref="Exception"><c>System.Exception</c></exception>
        public override void OnException(MethodExecutionArgs args)
        {
            Console.WriteLine("N/A");
        }
    }
}
