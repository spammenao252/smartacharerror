﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MainLib.AppSettings;

namespace MainLib
{
    public class CMainLib
    {
        /// <exception cref="Exception"></exception>
        public void Pick(string moduleName, int index)
        {
            Console.WriteLine("MainLib: " + Settings.Modules.UpTarget);

            if (moduleName == Settings.Modules.UpTarget)
                Console.WriteLine(Settings.Modules.UpTarget);
            else if (moduleName == Settings.Modules.StatDb)
                Console.WriteLine(Settings.Modules.StatDb);
            else if (moduleName == Settings.Modules.MainDb)
                Console.WriteLine(Settings.Modules.MainDb);
            else
            {
                throw new Exception(@"Unknown module name: " + moduleName);
            }
        }
    }
}
